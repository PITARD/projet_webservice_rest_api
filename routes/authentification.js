var config = require('config')
var passport = require('passport')
var TwitterStrategy = require('passport-twitter').Strategy

passport.use(new TwitterStrategy({
        consumerKey: config.consumerKey,
        consumerSecret: config.consumerSecret,
        callbackURL: config.callbackURL
    },
    function(token, tokenSecret, profile, cb) {
        User.findOrCreate({ twitterId: profile.id }, function (err, user) {
            return cb(err, user);
        });
    }
));
