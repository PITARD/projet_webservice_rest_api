var express = require('express');
var router = express.Router();
var Tweet = require('../models/tweet.js');


router.route('/mytweets')
	.post(function(req,res){
		var tweet = new Tweet(req.body);
		tweet.save(function(err) {
		    if (err) {
		      return res.send(err);
		    }
			res.json({ message: 'tweet Added' });
  		});
	});

router.route('/mytweets/:id')
	.get(function(req, res) {
			Tweet.find({user_id: req.params.id}, function (err, tweets){
				if (err) { throw err; }
				res.json(tweets);
			})
		})
	.delete(function(req, res){
		console.log(req)
		Tweet.remove({id : req.body.tweet_id, user_id : req.params.id},function (err) {
  			if (err) { throw err; }
  			res.json({message :'Tweet suprimés de la collection !'});
		});
	});
	
module.exports = router;
