var express = require('express');
var router = express.Router();
var User = require('../models/user.js');


router.route('/user')
	.post(function(req,res){
		var user = new User(req.body);
		user.save(function(err) {
		    if (err) {
		      return res.send(err);
		    }
			res.json({ message: 'user Added' });
  		});
	});

router.route('/user/:id')
	.get(function(req, res) {
			console.log(req.params);
			User.find({id: req.params.id}, function (err, tweets){
				if (err) { throw err; }
				console.log(tweets);
				res.json(tweets);
			})
		})
	.delete(function(req, res){
		User.remove({id : req.params.id},function (err) {
  			if (err) { throw err; }
  			res.json({message :'User suprimés de la collection !'});
		});
	});
module.exports = router;
