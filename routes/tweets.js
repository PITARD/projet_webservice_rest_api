var express = require('express');
var config = require('../config')
var router = express.Router();
var twitter = require('twitter');
var User = require('../models/user.js');
var moment = require('moment');

//Wraper de la fonction get qui permet de creer une promise
function get(url, params, client){
	return new Promise(function(resolve, reject){
		console.log(params)
		client.get(url, params, function(err, result, response){
			if(err) reject(err);
			resolve(result)
		})
	})
};

router.route('/tweets/:id')
    .get(function(req, res) {
	var params_user_id = req.params.id;

	var prom = User.findOne({ 'id': params_user_id }).exec()
	prom.then(function(user){
	 	var ids =[]
	  	var all_tweets =[]

	  	var params = {user_id : user.id, count : 200}
	  	var client = new twitter({
		  consumer_key: config.consumerKey,
		  consumer_secret: config.consumerSecret,
		  access_token_key: user.acces_token_key,
		  access_token_secret: user.acces_token_secret
		});
		client.get('friends/list.json', params, function(error, friends, response) {
			if(error) console.log(error);
		    friends = friends.users;
		    //map : associe une fonction a chaque element du tableau puis retourne un tableau avec les resultats
			var tweetsPromise = friends.map( friend => get('search/tweets.json', {q: 'from:'+friend.screen_name+' since:' + moment().format("YYYY-MM-DD"), count: 20}, client));
			//Recupère le resultat des toutes les promises lorsqu'elles ont terminer leurs execution.
			//arguments contient tout les resultats sous forme de tableau
			Promise.all(tweetsPromise).then(function(){
				for (var i = 0; i < arguments[0].length; i++) {
					if (((arguments[0])[i].statuses).length != 0)
						all_tweets.push((arguments[0])[i].statuses)
				}
				console.log(all_tweets.length);
				res.json(all_tweets);
			});
	    });
    });
});

module.exports = router;
