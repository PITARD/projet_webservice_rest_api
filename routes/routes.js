var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
    res.json({Message :"Welcome to the API"});
});
module.exports = router;
