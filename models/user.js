var mongoose=require('mongoose');
var Schema=mongoose.Schema;
require('mongoose-long')(mongoose);

var userSchema = new Schema({
  id : {
	  	type     : Schema.Types.Long,
	  	required : true,
	  	unique   : true,
},
  inscription_at : { type: Date, default: Date.now }, 
  url : String,
  name: String,
  profile_image_url: String,
  acces_token_key : String,
  acces_token_secret : String
});

module.exports = mongoose.model('User', userSchema);