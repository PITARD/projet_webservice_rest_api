var mongoose=require('mongoose');
var Schema=mongoose.Schema;
require('mongoose-long')(mongoose);

var tweetSchema = new Schema({
  	user_id: {
	  	type     : Schema.Types.Long,
	  	required : true,
	},
	id : {
	  	type     : Schema.Types.Long,
	  	required : true,
	},
	created_at : Date,
	urls : String,
	hashtags: [String],
	user_mentions: [String],
	text : String,
	retweeted : Boolean,
	user : Schema.Types.Mixed
});

module.exports = mongoose.model('Tweet', tweetSchema);
