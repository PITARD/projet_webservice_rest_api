var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var config = require('./config')
//insert of all routes file
var routes = require('./routes/routes.js');
var tweets = require('./routes/tweets.js'); //routes are defined here
var mytweets = require('./routes/mytweets.js');
var users = require('./routes/users.js');

var User = require('./models/user.js')
var app = express(); //Create the Express app


var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
}

app.use(allowCrossDomain);

//connect to database
var connectionString = 'mongodb://'+config.db.dbuser+':'+config.db.dbpassword+'@ds031257.mlab.com:31257/twitterapi'
mongoose.connect(connectionString);


//configure body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
//configure routes
app.use('/', routes);
app.use('/api', tweets); //This is our route middleware
app.use('/api', mytweets);
app.use('/api', users);

module.exports = app;
